/*
MIT License

Copyright (c) 2020 OES

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

require('dotenv').config();
const got = require('got');
const data = require('./data');

const canvasBaseURL = process.env.CANVAS_BASE_URL;
const canvasToken = process.env.CANVAS_TOKEN;
const getEndpoints = (params) => {
    return params.moduleItems.map(item => `${canvasBaseURL}/api/v1/courses/${params.courseId}/modules/${params.moduleId}/items/${item}/mark_read?as_user_id=${params.userId}`);
};

(async () => {
    for (user of data) {
        let endpoints = getEndpoints(user);
        for (url of endpoints) {
            try {
                await got.post({
                    url,
                    headers: {
                        Authorization: `Bearer ${canvasToken}`
                    }
                });
                console.log(`Successful | ${url}`);
            } catch (error) {
                console.log(`Failed | ${url}`, error);
            };
        };
    };
})();
