# MIT License

Copyright (c) 2020 OES

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# Pre-requisites
- LTS version of NodeJS (If nodejs is not already available) - https://nodejs.org/en/download/
- Any terminal of choice (or an integrated IDE eg. VS Code)
- Admin user token from Canvas - https://community.canvaslms.com/t5/Admin-Guide/How-do-I-manage-API-access-tokens-as-an-admin/ta-p/89

# Installing dependencies
Once NodeJS is installed. Unzip the package content into a desired FOLDER. 
Using the terminal, navigate in to the folder
run the following command to install dependencies

```sh
npm install
```

# Setting Canvas credentials
Create a **.env** file within the package (use the .env-sample file as a template) and populate the variables with the canvas information (URL and token)

```sh
CANVAS_BASE_URL=https://{INSTITUTION}.instructure.com
CANVAS_TOKEN=11146~SH4P1PkwPCAlOzTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

# Executing the script
## IMPORTANT: Please make sure the data file is populated with the correct data before executing the script (Refer to "Data requirements" below)
**It is recommended that you execute this script against a test canvas environment first**

Once the data set is ready and the credentials are set, execute the script using the following command in the terminal.

```sh
c:/FOLDER/node index
```

# Data requirements
Populate the **data/index.json** file with the canvas id information for each user. Follow the JSON standard notation. The data structure is an array of objects. Each object must contain information for a single user. There can be multiple user objects. The canvas ID can be obtained by inspecting the elements in the canvas UI. *Note the module Items field is an array.

```sh
data/index.json
```

```sh
[
    {
        "userId": "Canvas_User_Id_1",
        "courseId": "Canvas_Course_Id_1",
        "moduleId": "Canvas_Module_Id_1",
        "moduleItems": [
            "module_item_id_1",
            "module_item_id_2",
            "module_item_id_3",
            "module_item_id_4",
        ]
    },
    .
    .
    .
]
```
